from django import forms
from core.models import FoodItem


class FoodItemForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-input'}))
    cost = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-input'}))
    food_desc = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-input'}))
    preparing_time = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-input'}))
    img_path = forms.CharField(widget=forms.FileInput(attrs={'class': 'form-input'}))
    # category = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-input'}))
    CHOICES = (('Option 1', 'Option 1'), ('Option 2', 'Option 2'),)

    OPTIONS = (
        ("N", "North Indian"),
        ("S", "South Indian"),
        ("E", "East Indian"),

    )
    # category = forms.MultipleChoiceField(widget=forms.SelectMultiple,
    #                                       choices=OPTIONS)

    def save(self):
        pass

    class Meta:
        model = FoodItem
        fields = ('name', 'cost', 'food_desc', 'preparing_time', 'img_path' )



class EditFoodItemForm(forms.ModelForm):
    name =forms.CharField(widget=forms.TextInput(attrs={'class': 'form-input'}))
    cost = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-input'}))
    food_desc = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-input'}))
    preparing_time = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-input'}))

    class Meta:
        model = FoodItem
        fields = ('name', 'cost', 'food_desc', 'preparing_time' )