from  core.models import Restaurant,FoodItem,Category,Order,OrderedItem
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodItem
        fields = '__all__'


class FoodItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = FoodItem
        fields = '__all__'
        depth = 1


class RestaurantSerializer(serializers.ModelSerializer):
    fooditem_set=FoodItemSerializer(many=True, required=False)

    class Meta:
        model=Restaurant
        fields = '__all__'


class OrderStatus(serializers.ModelSerializer):
    class Meta:
        model=Order
        fields = '__all__'
        depth = 1



class OrderedItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderedItem
        fields = '__all__'
        depth = 1


class RestaurantOrderSerializer(serializers.ModelSerializer):
    ordereditem_set=OrderedItemSerializer(many=True, required=True)

    class Meta:
        model=Order
        fields = ['status','ordereditem_set']
        depth = 1


# class AttrPKField(serializers.PrimaryKeyRelatedField):
#     def get_queryset(self):
#         staus = self.context['request'].user
#         staus = Attribute.objects.filter(user=user)
#         return queryset
