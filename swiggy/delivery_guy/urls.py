from django.urls import path
from . import views as delivery_views

urlpatterns=[
    path('delivery/dashboard/<int:pk>', delivery_views.delivery_guy_dashboard, name='delivery_dashboard'),
    path('order_detail/<int:pk>', delivery_views.delivery_order, name='order_detail'),
    path('get_location/<int:pk>', delivery_views.get_location, name='get_location'),
    path('order_status/<int:pk>', delivery_views.order_status, name='order_status')
]